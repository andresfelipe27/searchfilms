import React, { Component } from 'react';
import logo from './logo.svg';
import SelectFims from './components/selectFilms';
import ModalFilms from './components/modalFilms'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
           <SelectFims  />
      </div>
    );
  }
}

export default App;
