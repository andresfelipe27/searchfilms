import React, { Component } from 'react';

import ListFilms from './ListFilms';
import ModalFilms from './modalFilms';
import '../css/listFilms.css'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

var URL = 'http://www.omdbapi.com';
var KEY = '/?apikey=9c29485e';

class SelectFimls extends Component {

  constructor(props) {
    super(props);

    this.state = {
      movie: '',
      listMovies: [],
      typeQuery: 'movie',
    }
    this.HandleState = this.HandleState.bind(this);
    this.handleFilms = this.HandleFilms.bind(this);
  }


  Handlenotify = (typeQuery) => {

    toast.error("Actualmente no hay existencias de " + typeQuery + " con la palabra " + this.state.movie, {
      position: toast.POSITION.BOTTOM_CENTER
    });

  }
  HandleState(event) {
    this.setState({
      movie: event.target.value

    })
    if (this.state.movie.length > 3) {
      this.HandleFilms(this.state.movie);
    }

  };
  HandleFilms(movie) {
    fetch(URL + KEY + '&s=' + movie)
      .then(response => response.json())
      .then(response => {
        let log = document.getElementById('log');
        if (response.Error) {
          this.Handlenotify('peliculas');

        } else {

          this.setState({

            listMovies: response.Search

          })
        }

      })
  }



  HandleFilmsCategory(typeQuery) {
    fetch(URL + KEY + '&s=' + this.state.movie + '&type=' + typeQuery)
      .then(response => response.json())
      .then(response => {

        if (response.Error) {
          this.Handlenotify(typeQuery);

        } else {
          this.setState({

            listMovies: response.Search

          })
        }
      })

  }
  render() {
    return (
      <div>
        <nav className={this.state.movie.length > 3 ? "navbar navbar-expand-lg navbar-light  smallVar" : "navbar navbar-expand-lg navbar-light  bigVar"}>
          <div className="">
            <a className={this.state.movie.length > 3 ? "navbar-brand  brandTitle" : "navbar-brand brandTitle titlecenter"} href="#">PipeFilms</a>
          </div>
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav mr-auto"></ul>
            <form className={this.state.movie.length > 3 ? "" : "formCenter"}>
              <input onChange={this.HandleState} className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
              <div>
                <label className="check checkMovie">
                  <input onChange={() => { this.HandleFilmsCategory("movie") }} type="radio" name="type" />
                  Movie
                      </label>
                <label className="check checkSeries">
                  <input onChange={() => { this.HandleFilmsCategory("series") }} type="radio" name="type" />
                  Serie
                      </label>
              </div>
            </form>
          </div>
        </nav>
        <div className="container-fluid">
          <div className="row">
            <ListFilms data={this.state.listMovies} />
          </div>
          <ToastContainer />
        </div>
      </div>



    )

  }




}

export default SelectFimls;
