import React, { Component } from 'react';
import '../css/listFilms.css'
import '../css/tableDescription.css'
import ModalFilms from './modalFilms'
import Rating from './Rating'
import { Modal, Button } from 'react-bootstrap';
var URL = 'http://www.omdbapi.com';
var KEY = '/?apikey=9c29485e';
class ListFilms extends Component {

  constructor(props) {
    super(props);


    this.state = {
      isOpen: false,
      infoFilm: []
    }
  }


  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  handleSerachId(id) {
    fetch(URL + KEY + '&i=' + id)
      .then(response => response.json())
      .then(response => {
        this.setState({

          infoFilm: response

        })
      })
  }

  render() {

    return (
      <div className="card-deck">
          {this.props.data.map((item, i) => (
            <div key={i} id={item.imdbID}>
              <div className="card"><img src={item.Poster} />
                <div className="info">
                  <h1>{item.Title}</h1>
                  <p>{item.Year}</p>
                  <button onClick={() => { this.toggleModal(this.handleSerachId(item.imdbID)) }} className="btn btn-info">Leer Mas</button>
                </div>
              </div>
            </div>
          ))}
        <div>
          <Modal show={this.state.isOpen}
            onHide={this.toggleModal} className="modalDescr" >
            <Modal.Body >
              <div class='cardDescripction'>
                <div class='card_left'>
                  <img src={this.state.infoFilm.Poster} />
                </div>
                <div class='card_right'>
                  <h4>{this.state.infoFilm.Title}</h4>
                  <div class='card_right__details'>
                    <ul>
                      <li>{this.state.infoFilm.Year}</li>
                      <li>{this.state.infoFilm.Runtime}</li>
                      <li>{this.state.infoFilm.Genre}</li>
                    </ul>
                    <div class='card_right__rating'>
                      <div class='card_right__rating__stars'>
                        <p className="actors">{this.state.infoFilm.Actors}</p>
                        <div>
                          <Rating num={this.state.infoFilm.imdbRating} />
                        </div>
                      </div>
                    </div>
                    <div class='card_right__review'>
                      <p>{this.state.infoFilm.Plot}</p></div>
                    <div class='card_right__button'>
                      <Button variant="success" onClick={this.toggleModal}>
                        Close
                       </Button>
                    </div>
                  </div>
                </div>
              </div>

            </Modal.Body>
            <Modal.Footer>
              <Button variant="success" onClick={this.toggleModal}>
                Close
            </Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    );
  }


}


export default ListFilms;
