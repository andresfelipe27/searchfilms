import React, { Component } from 'react';

const STAR_IMAGE = "http://images.clipartpanda.com/clipart-star-yckBEbKcE.png",
      EMPTY_STAR_IMAGE = "https://vignette.wikia.nocookie.net/animal-jam-clans-1/images/c/c1/Star_star_.png/revision/latest?cb=20170111070537"

class Ratings extends React.Component {
    constructor(props){
      super(props);

    }


  renderStars(num) {
    let stars = Array.apply(null, new Array(10))
    return stars.map((star, index) => 
      <img
        style={{width: '30px'}}
        src={ index < num ? STAR_IMAGE: EMPTY_STAR_IMAGE}
      />
    )
  }
                     
  altRenderStars(num) {
      let stars = []
      for(let i = 0; i < 5; i++) {
        stars.push(<img
        style={{width: '30px'}}
        src={ i < num ? STAR_IMAGE: EMPTY_STAR_IMAGE}
      />)
      }
      return stars
    }
  
  render() {
    return (
      <div>
        {this.renderStars(this.props.num)}
        <br />
      </div>
    )
  }
}


 
export default Ratings;